// Copyright (c) Microsoft. All rights reserved. Licensed under the MIT license. See LICENSE.txt in the project root for license information.
var express = require('express');
var router = express.Router();
var authHelper = require('../helpers/auth');
var graph = require('@microsoft/microsoft-graph-client');
var moment = require('moment');
var request = require('request');

/* GET /rooms */
router.get('/', async function (req, res, next) {
  let parms = {
    title: 'Rooms',
    active: {
      rooms: true
    }
  };

  const accessToken = await authHelper.getAccessToken(req.cookies, res);
  const userName = req.cookies.graph_user_name;

  if (accessToken && userName) {
    parms.user = userName;

    // Initialize Graph client
    const client = graph.Client.init({
      authProvider: (done) => {
        done(null, accessToken);
      }
    });

    // Set start of the calendar view to today at midnight
    const end = new Date(new Date());
    // Set end of the calendar view to 7 days from start
    const start = new Date(new Date(end).setDate(end.getDate() - 5));

    try {
      // Get the first 10 events for the coming week
      const result = await client
        .api(`/me/calendar/getSchedule?startTime=&endTime=${end.toISOString()}`)
        .version("beta")
        .post({
          "Schedules": ["marconi@amis.nl", "rcvvuurtoren@amis.nl", "redwood@amis.nl", "Kamer0.06@amis.nl"],
          "StartTime": {
            "dateTime": `${start.toISOString()}`
          },
          "endTime": {
            "dateTime": `${end.toISOString()}`
          },
          // "availabilityViewInterval": "15"
        });

      parms.debug = JSON.stringify(result.value);

      parms.events = result.value;
      var query = "";

      result.value.forEach(function (email) {
        switch (email.scheduleId) {
          case "marconi@amis.nl":
            room = "Marconi";
            break;
          case "rcvvuurtoren@amis.nl":
            room = "Vuurtoren";
            break;
          case "redwood@amis.nl":
            room = "Redwood";
            break;
          case "Kamer0.06@amis.nl":
            room = "Huiskamer";
            break;
          default:
            room = "unknown";
            break;
        }

        email.scheduleItems.forEach(function (afspraak) {

          console.log([afspraak.start.dateTime, afspraak.end.dateTime]);
          console.log(room + " :" + afspraak.subject);

          [afspraak.start.dateTime, afspraak.end.dateTime].forEach(function (tijd, i) {
            query += room +
              ',SensorType="OutlookReservatie"' +
              ' Building="Nieuwegein"' +
              ',Location="Edinsonbaan 15, Nieuwegein"' +
              ',Floor=1' +
              ',Description="' + 'Van ' + moment(afspraak.start.dateTime).add(1, "hours").format("HH:mm") + ' tot ' + moment(afspraak.end.dateTime).add(1, "hours").format("HH:mm") + ' - ' + afspraak.subject +
              '",SensorValue=' + (i ? 0 : 1) +
              // Converts measurement time to milliseconds and multiplies it for nanoseconds, otherwise influxDB can't read it.
              // The "/n" is for influxdb to read there is another entry, multiple lines can be written at once.
              ' ' + moment(tijd).add(1, "hours").format("x") * 1000000 + "\n";
          })


        });
      })

      parms.debug = query;

      if (req.query.post) {
        sendMsgToInflux(query, function (resp) {
          parms.response = (resp.statusCode == 204) ? "Success!" : "Error!";
          res.render('rooms', parms);
        });
      } else {
        res.render('rooms', parms);
      }
    } catch (err) {
      parms.message = 'Error retrieving events';
      parms.error = {
        status: `${err.code}: ${err.message}`
      };
      parms.debug = JSON.stringify(err.body, null, 2);
      res.render('error', parms);
    }

  } else {
    // Redirect to home
    res.redirect('/');
  }
});

// http://influxpof.westeurope.cloudapp.azure.com:8086/write?db=AMISKantoor&u=sam&p=mas
// sendMsgToInflux makes a POST request to the InfluxDB
function sendMsgToInflux(q, callback) {
  var requestData = request({
    headers: {
      'Content-Type': 'application/json'
    },
    uri: "http://influxpof.westeurope.cloudapp.azure.com:8086/write?db=AMISKantoor&u=sam&p=mas",
    body: q,
    method: 'POST'
  }, function (err, res, body) {
    console.log("LOG: " + JSON.stringify(err) + JSON.stringify(res) + JSON.stringify(body));
    callback(res);
  });
}
module.exports = router;
